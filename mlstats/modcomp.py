#!/usr/bin/env python

from collections import namedtuple
from itertools import combinations
import logging

import numpy as np
import pandas as pd
from scipy.stats import binom
from scipy.stats.distributions import chi2


def mcnemar_exact(agent_a: np.ndarray,
                  agent_b: np.ndarray,
                  mid_p_correction: bool = False) -> tuple:
    """ McNemar's exact marginal homogeneity test

    Determine whether two binary-classification models have the same
    distribution of predictions.

    :param agent_a: a vector of length N with agent_a[i] the prediction for
                    sample i by agent A
    :param agent_b: a vector of length N with agent_b[i] the prediction for
                    sample i by agent B
    :param mid_p_correction: whether to apply mid-p correction
    :returns: a tuple with the p-value

    Fagerland, Morten W., Stian Lydersen, and Petter Laake. "The McNemar test
    for binary matched-pairs data: mid-p and asymptotic are better than exact
    conditional." (2013).
    """
    assert agent_a.ndim == 1 and agent_b.ndim == 1\
            and agent_a.shape[0] == agent_b.shape[0]

    df = pd.DataFrame(np.stack([agent_a, agent_b]).T)
    ctable = pd.crosstab(df[0], df[1])

    n12 = ctable.iloc[0,1]
    n21 = ctable.iloc[1,0]

    p_value = 2 * binom.cdf(n=n12+n21, k=n12, p=0.5)
    if mid_p_correction:
        p_value -= binom.pmf(n=n12+n21, k=n12, p=0.5)

    return namedtuple('TestResults', ['p'])(p_value)

def mcnemar(agent_a: np.ndarray,
            agent_b: np.ndarray,
            continuity_correction: bool = False) -> tuple:
    """ McNemar's asymptotic marginal homogeneity test

    Determine whether two binary-classification models have the same
    distribution of predictions.

    :param agent_a: a vector of length N with agent_a[i] the prediction for
                    sample i by agent A
    :param agent_b: a vector of length N with agent_b[i] the prediction for
                    sample i by agent B
    :param continuity_correction: whether to correct for continuity [Edwards]
    :returns: a tuple with the Q statistic and the corresponding p-value


    EXAMPLE [Edwards, 1948]:

    agent_a = np.repeat(np.array([0,1], dtype=np.int32), [100, 100])
    agent_b = np.repeat(np.tile([0,1], 2), [20, 80, 60, 40])

    in : mcnemar(agent_a, agent_b)
    out: 0.009823274507519235

    in : mcnemar(agent_a, agent_b, continuity_correction=True)
    out: 0.014171388254012323


    REFERENCES:

    McNemar, Quinn. 'Note on the Sampling Error of the Difference between
    Correlated Proportions or Percentages'. Psychometrika 12, no. 2 (1947):
    153–157.

    Edwards, Allen L. 'Note on the "Correction for Continuity" in Testing
    the Significance of the Difference between Correlated Proportions'.
    Psychometrika 13, no. 3 (1948): 185–187.

    """
    assert agent_a.ndim == 1 and agent_b.ndim == 1\
            and agent_a.shape[0] == agent_b.shape[0]

    df = pd.DataFrame(np.stack([agent_a, agent_b]).T)
    ctable = pd.crosstab(df[0], df[1])

    A = ctable.iloc[0,0]
    D = ctable.iloc[1,1]

    if A + D < 25:
        logging.warning("Too few samples to accurately approximate"
                        " the chi-squared distribution. Consider using"
                        " a binomial test.")

    if continuity_correction:
        Q = ((abs(D - A) - 1) ** 2) / (D + A)
    else:
        Q = ((D - A) ** 2) / (D + A)

    p_value = chi2.sf(Q, df=1)

    return namedtuple('TestResults', ['p', 'Q'])(p_value, Q)

def stuart_maxwell(agent_a: np.ndarray,
                   agent_b: np.ndarray,
                   num_categories: int) -> tuple:
    """ Stuart-Maxwell marginal homogeneity test

    Determine whether two multi-class models have the same distribution of
    predictions. A generalization of McNemar's test.

    :param agent_a: a vector of length N with agent_a[i] the prediction for
                    sample i by agent A
    :param agent_b: a vector of length N with agent_b[i] the prediction for
                    sample i by agent B
    :param num_categories: the total number of categories (classes)
    :returns: a tuple with the Q statistic and the corresponding p-value


    EXAMPLE [Stuart, 1955]:

    agent_a = np.repeat(np.array([0,1,2,3], dtype=np.int32), [1976, 2256,
                                                              2456, 789])
    agent_b = np.repeat(np.tile([0,1,2,3], 4), [1520, 266, 124, 66,
                                                234,  1512, 432, 78,
                                                117, 362, 1772, 205,
                                                36, 82, 179, 492])

    in : stuart_maxwell(agent_a, agent_b, num_categories=4)
    out: 0.007533425054800539


    EXAMPLE [Maxwell, 1970]:

    agent_a = np.repeat(np.array([0,1,2], dtype=np.int32), [7, 9, 4])
    agent_b = np.repeat(np.tile([0,1,2], 3), [5, 2, 0, 1, 6, 2, 0, 3, 1])

    in : stuart_maxwell(agent_a, agent_b, num_categories=3)
    out: 0.7659283383646487

    REFERENCES:

    Stuart, Alan. 'A Test for Homogeneity of the Marginal Distributions in a
    Two-Way Classification'. Biometrika 42, no. 3/4 (1955): 412–16.
    https://doi.org/10.2307/2333387.

    Maxwell, A. E. 'Comparing the Classification of Subjects by Two Independent
    Judges'. The British Journal of Psychiatry 116, no. 535 (June 1970): 651–55.
    https://doi.org/10.1192/bjp.116.535.651.

    """
    assert agent_a.ndim == 1 and agent_b.ndim == 1\
            and agent_a.shape[0] == agent_b.shape[0]\
            and num_categories >= 1

    df = pd.DataFrame(np.stack([agent_a, agent_b]).T)
    ctable = pd.crosstab(df[0], df[1]).values

    # create (co-)variance table
    vtable = np.zeros((num_categories - 1, num_categories - 1), dtype=np.int64)
    marginal_vars = ctable.sum(axis=1) + ctable.sum(axis=0) - 2 * ctable.diagonal()
    np.fill_diagonal(vtable, marginal_vars[:-1])

    non_diag_coors = list(combinations(np.arange(num_categories - 1), r=2))
    for coor in non_diag_coors:
        coor_inv = coor[::-1]
        cov = -(ctable[coor] + ctable[coor_inv])

        vtable[coor] = cov
        vtable[coor_inv] = cov

    # compute chi square statistic
    vtable_inv = np.linalg.inv(vtable)
    marginal_diffs = ctable.sum(axis=1)[:-1] - ctable.sum(axis=0)[:-1]
    Q = sum(vtable_inv.diagonal() * marginal_diffs ** 2)\
        + 2 * sum([vtable_inv[coor]
                   * marginal_diffs[coor[0]] * marginal_diffs[coor[1]]
                   for coor in non_diag_coors])

    p_value = chi2.sf(Q, df=num_categories-1)

    return namedtuple('TestResults', ['p', 'Q'])(p_value, Q)

def mcnemar_bowker(agent_a: np.ndarray,
                   agent_b: np.ndarray,
                   num_categories: int) -> tuple:
    """ McNemar-Bowker symmetry test

    Determine whether two multi-class models have the same distribution of
    predictions. A generalization of McNemar's test.

    :param agent_a: a vector of length N with agent_a[i] the prediction for
                    sample i by agent A
    :param agent_b: a vector of length N with agent_b[i] the prediction for
                    sample i by agent B
    :param num_categories: the total number of categories (classes)
    :returns: a tuple with the Q statistic and the corresponding p-value
    """

    raise NotImplementedError()

def cochran_q(agents: np.ndarray) -> tuple:
    """ Cochran's Q test

    Determine whether M multi-class models have the same distribution of correct/incorrect
    predictions. A generalization of McNemar's test.

    :param agents: a MxN matrix for N samples from M agents with agents[m,i] = 1
                   if agent m classified sample i correctly, and agents[m,i] = 0
                   otherwise.
    :returns: a tuple with the Q statistic and the corresponding p-value


    EXAMPLE [Cochran, 1950]:

    agents = np.stack([np.repeat(np.array([1,0], dtype=np.int8), [6,63]),
                       np.repeat(np.array([1,0], dtype=np.int8), [10,59]),
                       np.repeat(np.array([1,0,1,0], dtype=np.int8), [4,2,3,60]),
                       np.repeat(np.array([1,0], dtype=np.int8), [10,59])])

    in : cochran_q(agents)
    out: 0.04493640116781306


    REFERENCES:

    Cochran, W. G. 'The Comparison of Percentages in Matched Samples'. Biometrika 37, no.
    3/4 (1950): 256–66. https://doi.org/10.2307/2332378.
    """
    assert agents.ndim == 2 and agents.shape[0] >= 2

    M = agents.shape[0]
    total_tp = np.sum(agents)  # total true positives

    # numerator
    Tj = np.sum(np.sum(agents, axis=1) ** 2)
    T_bar = (total_tp ** 2) / M
    numerator = M * (M-1) * (Tj - T_bar)

    # denumerator
    unique, counts = np.unique(np.sum(agents, axis=0), return_counts=True)
    denumerator = M * total_tp - np.sum(counts * unique ** 2)

    # Q
    Q = numerator / denumerator
    p_value = chi2.sf(Q, df=M-1)

    return namedtuple('TestResults', ['p', 'Q'])(p_value, Q)
