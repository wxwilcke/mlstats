#!/usr/bin/env python

from collections import namedtuple

import numpy as np
from scipy.stats import f, t


def alpaydin_5x2cv_F_test(agent_a: np.ndarray,
                          agent_b: np.ndarray) -> tuple:
    """ Alpaydin's Combined 5x2-Fold Cross-Validated Paired F-test

    Determine whether two algorithms have the same error-rate distribution.

    :param agent_a: a 5x2 array with on (i,j) the accuracy of agent A on
                    outer fold i and inner fold j
    :param agent_b: a 5x2 array with on (i,j) the accuracy of agent B on
                    outer fold i and inner fold j
    :returns: a tuple with the F statistic and the corresponding p-value


    REFERENCES:

    Alpaydm, Ethem. 'Combined 5x2 Cv F Test for Comparing Supervised
    Classification Learning Algorithms'. Neural Computation 11, no. 8
    (1999): 1885–1892.

    """
    assert agent_a.shape == (5, 2) and agent_b.shape == (5, 2)

    diff = agent_a - agent_b

    # numerator
    numerator = np.sum(diff ** 2)

    # denumerator
    var = 2 * np.var(diff, axis=1)
    denumerator = 2 * np.sum(var)

    # F statistic
    F = numerator / denumerator
    p_value = f.sf(F, dfn=10, dfd=5)

    return namedtuple('TestResults', ['p', 'F'])(p_value, F)

def dietterich_5x2cv_t_test(agent_a: np.ndarray,
                            agent_b: np.ndarray) -> tuple:
    """ Dietterich's 5x2-Fold Cross-Validated Paired t-test

    Determine whether two algorithms have the same error-rate distribution.

    :param agent_a: a 5x2 array with on (i,j) the accuracy of agent A on
                    outer fold i and inner fold j
    :param agent_b: a 5x2 array with on (i,j) the accuracy of agent B on
                    outer fold i and inner fold j
    :returns: a tuple with the T statistic and the corresponding p-value


    REFERENCES:

    Dietterich, Thomas G. `Approximate Statistical Tests for Comparing
    Supervised Classification Learning Algorithms'. Neural Computation
    10, no. 7 (1998): 1895–1923.
    """
    assert agent_a.shape == (5, 2) and agent_b.shape == (5, 2)

    diff = agent_a - agent_b

    # numerator
    numerator = diff[0,0]

    # denumerator
    var = 2 * np.var(diff, axis=1)
    denumerator = np.sqrt(np.sum(var)/5)

    # t statistic
    T = numerator / denumerator
    p_value = t.sf(T, df=5)

    return namedtuple('TestResults', ['p', 'T'])(p_value, T)
