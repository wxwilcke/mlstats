#!/usr/bin/env python

from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='mlstats',
    version='0.1',
    author='Xander Wilcke',
    author_email='w.x.wilcke@vu.nl',
    url='https://gitlab.com/wxwilcke/mlstats',
    description='Simple statistics for machine learning',
    license='GLP3',
    include_package_data=True,
    zip_safe=True,
    install_requires=[
        'numpy',
        'scipy',
        'pandas'
    ],
    packages=['mlstats'],
)
