#!/usr/bin/env python

import unittest

import numpy as np

from mlstats.modcomp import (mcnemar,
                             mcnemar_exact,
                             mcnemar_bowker,
                             stuart_maxwell,
                             cochran_q)


class TestMcNemar(unittest.TestCase):
    def setUp(self):
        self.agent_a = np.repeat(np.array([0,1], dtype=np.int32), [100, 100])
        self.agent_b = np.repeat(np.tile([0,1], 2), [20, 80, 60, 40])

    def test_mcnemar_1947(self):
        p, _ = mcnemar(self.agent_a,
                       self.agent_b,
                       continuity_correction=False)
        self.assertAlmostEqual(p, 9.8e-3, places=4)

    def test_edwards_1948(self):
        p, _ = mcnemar(self.agent_a,
                       self.agent_b,
                       continuity_correction=True)
        self.assertAlmostEqual(p, 1.42e-2, places=4)

    def tearDown(self):
        del self.agent_a
        del self.agent_b

class TestStuartMaxwell(unittest.TestCase):
    def setUp(self):
        self.agent_a_1955 = np.repeat(np.array([0,1,2,3], dtype=np.int32), [1976, 2256,
                                                                            2456, 789])
        self.agent_b_1955 = np.repeat(np.tile([0,1,2,3], 4), [1520, 266, 124, 66,
                                                              234,  1512, 432, 78,
                                                              117, 362, 1772, 205,
                                                              36, 82, 179, 492])
        self.num_categories_1955 = 4

        self.agent_a_1970 = np.repeat(np.array([0,1,2], dtype=np.int32), [7, 9, 4])
        self.agent_b_1970 = np.repeat(np.tile([0,1,2], 3), [5, 2, 0, 1, 6, 2, 0, 3, 1])
        self.num_categories_1970 = 3

    def test_stuart_1955(self):
        p, _ = stuart_maxwell(self.agent_a_1955,
                              self.agent_b_1955,
                              self.num_categories_1955)
        self.assertAlmostEqual(p, 7.5e-3, places=4)

    def test_maxwell_1970(self):
        p, _ = stuart_maxwell(self.agent_a_1970,
                              self.agent_b_1970,
                              self.num_categories_1970)
        self.assertAlmostEqual(p, 0.7659, places=4)

    def tearDown(self):
        del self.agent_a_1955
        del self.agent_b_1955
        del self.num_categories_1955

        del self.agent_a_1970
        del self.agent_b_1970
        del self.num_categories_1970

class TestConchranQ(unittest.TestCase):
    def setUp(self):
        self.agents = np.stack([np.repeat(np.array([1,0], dtype=np.int8), [6,63]),
                                np.repeat(np.array([1,0], dtype=np.int8), [10,59]),
                                np.repeat(np.array([1,0,1,0], dtype=np.int8), [4,2,3,60]),
                                np.repeat(np.array([1,0], dtype=np.int8), [10,59])])


    def test_cochran_1950(self):
        p, _ = cochran_q(self.agents)
        self.assertAlmostEqual(p, 4.49e-2, places=4)

    def tearDown(self):
        del self.agents

if __name__ == '__main__':
    unittest.main()

