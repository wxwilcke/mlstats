# Simple Statistics for Machine Learning

A python package for calculating the significance of machine learning experiments

Includes:
- McNemar's asymptotic marginal homogeneity test
- McNemar's exact marginal homogeneity test
- Stuart-Maxwell marginal homogeneity test
- McNemar-Bowker symmetry test
- Cochran's Q test
- Dietterich's 5x2-Fold Cross-Validated Paired t-test
- Alpaydin's Combined 5x2-Fold Cross-Validated Paired F-test
